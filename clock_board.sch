EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x25_Odd_Even J1
U 1 1 5FD62DCD
P 1500 2850
F 0 "J1" H 1550 4267 50  0000 C CNN
F 1 "Conn_01x40" H 1550 4176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x25_P2.54mm_Horizontal" H 1500 2850 50  0001 C CNN
F 3 "~" H 1500 2850 50  0001 C CNN
	1    1500 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5FD64472
P 2000 2750
F 0 "#PWR0101" H 2000 2600 50  0001 C CNN
F 1 "+5V" H 2015 2923 50  0000 C CNN
F 2 "" H 2000 2750 50  0001 C CNN
F 3 "" H 2000 2750 50  0001 C CNN
	1    2000 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2750 1800 2750
$Comp
L power:+5V #PWR0102
U 1 1 5FD65A34
P 1000 2850
F 0 "#PWR0102" H 1000 2700 50  0001 C CNN
F 1 "+5V" H 1015 3023 50  0000 C CNN
F 2 "" H 1000 2850 50  0001 C CNN
F 3 "" H 1000 2850 50  0001 C CNN
	1    1000 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5FD65E00
P 2000 2850
F 0 "#PWR0103" H 2000 2600 50  0001 C CNN
F 1 "GND" H 2005 2677 50  0000 C CNN
F 2 "" H 2000 2850 50  0001 C CNN
F 3 "" H 2000 2850 50  0001 C CNN
	1    2000 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2850 1800 2850
$Comp
L power:GND #PWR0104
U 1 1 5FD66D1A
P 850 2700
F 0 "#PWR0104" H 850 2450 50  0001 C CNN
F 1 "GND" H 855 2527 50  0000 C CNN
F 2 "" H 850 2700 50  0001 C CNN
F 3 "" H 850 2700 50  0001 C CNN
	1    850  2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 2750 1100 2700
Wire Wire Line
	1100 2700 850  2700
Wire Wire Line
	1100 2750 1300 2750
Text GLabel 1300 1650 0    50   Input ~ 0
A0
Text GLabel 1300 1750 0    50   Input ~ 0
A1
Text GLabel 1300 1850 0    50   Input ~ 0
A2
Text GLabel 1300 1950 0    50   Input ~ 0
A3
Text GLabel 1300 2050 0    50   Input ~ 0
A4
Text GLabel 1300 2150 0    50   Input ~ 0
A5
Text GLabel 1300 2250 0    50   Input ~ 0
A6
Text GLabel 1300 2350 0    50   Input ~ 0
A7
Text GLabel 1300 2450 0    50   Input ~ 0
A8
Text GLabel 1300 2550 0    50   Input ~ 0
A9
Text GLabel 1300 2650 0    50   Input ~ 0
A10
Text GLabel 1300 2950 0    50   Input ~ 0
A11
Text GLabel 1300 3050 0    50   Input ~ 0
A12
Text GLabel 1300 3150 0    50   Input ~ 0
A13
Text GLabel 1300 3250 0    50   Input ~ 0
A14
Text GLabel 1300 3350 0    50   Input ~ 0
A15
Text GLabel 1800 1650 2    50   BiDi ~ 0
D0
Text GLabel 1800 1750 2    50   BiDi ~ 0
D1
Text GLabel 1800 1850 2    50   BiDi ~ 0
D2
Text GLabel 1800 1950 2    50   BiDi ~ 0
D3
Text GLabel 1800 2050 2    50   BiDi ~ 0
D4
Text GLabel 1800 2150 2    50   BiDi ~ 0
D5
Text GLabel 1800 2250 2    50   BiDi ~ 0
D6
Text GLabel 1800 2350 2    50   BiDi ~ 0
D7
Text GLabel 1300 4050 0    50   Input ~ 0
~RESET~
Text GLabel 1800 4050 2    50   BiDi ~ 0
~NMI~
Text GLabel 750  3450 0    50   Input ~ 0
RDY
Text GLabel 750  3550 0    50   Input ~ 0
BE
Text GLabel 1300 3650 0    50   Input ~ 0
CLK
Text GLabel 1300 3750 0    50   Input ~ 0
R~W~
Text GLabel 1300 3950 0    50   Input ~ 0
SYNC
Text GLabel 750  3850 0    50   BiDi ~ 0
~IRQ~
Text GLabel 1800 3950 2    50   BiDi ~ 0
~IRQ0~
Text GLabel 1800 3850 2    50   BiDi ~ 0
~IRQ1~
Text GLabel 1800 3750 2    50   BiDi ~ 0
~IRQ2~
Text GLabel 1800 3650 2    50   BiDi ~ 0
~IRQ3~
Wire Wire Line
	750  3850 1300 3850
Wire Wire Line
	750  3450 1300 3450
Wire Wire Line
	750  3550 1300 3550
Text GLabel 1800 2950 2    50   BiDi ~ 0
EX3
Text GLabel 1800 3050 2    50   BiDi ~ 0
~INH~
Text GLabel 1800 3150 2    50   BiDi ~ 0
~SLOT_SEL~
Text GLabel 1800 3250 2    50   BiDi ~ 0
LED1
Text GLabel 1800 3350 2    50   BiDi ~ 0
LED2
Text GLabel 1800 3450 2    50   BiDi ~ 0
LED3
Text GLabel 1800 3550 2    50   BiDi ~ 0
LED4
Text GLabel 1800 2450 2    50   BiDi ~ 0
EX0
Text GLabel 1800 2550 2    50   BiDi ~ 0
EX1
Text GLabel 1800 2650 2    50   BiDi ~ 0
EX2
Wire Wire Line
	1000 2850 1300 2850
$Comp
L Oscillator:CXO_DIP8 X1
U 1 1 5FEEF403
P 4650 2400
F 0 "X1" H 4994 2446 50  0000 L CNN
F 1 "CXO_DIP8" H 4994 2355 50  0000 L CNN
F 2 "Oscillator:Oscillator_DIP-8" H 5100 2050 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/B400/OSZI.pdf" H 4550 2400 50  0001 C CNN
	1    4650 2400
	1    0    0    -1  
$EndComp
$Comp
L Oscillator:CXO_DIP8 X2
U 1 1 5FEEF711
P 4650 4350
F 0 "X2" H 4994 4396 50  0000 L CNN
F 1 "CXO_DIP8" H 4994 4305 50  0000 L CNN
F 2 "Oscillator:Oscillator_DIP-8" H 5100 4000 50  0001 C CNN
F 3 "http://cdn-reichelt.de/documents/datenblatt/B400/OSZI.pdf" H 4550 4350 50  0001 C CNN
	1    4650 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5FEF0904
P 4650 3850
F 0 "#PWR0105" H 4650 3700 50  0001 C CNN
F 1 "+5V" H 4665 4023 50  0000 C CNN
F 2 "" H 4650 3850 50  0001 C CNN
F 3 "" H 4650 3850 50  0001 C CNN
	1    4650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4050 4650 3850
$Comp
L Device:C_Small C2
U 1 1 5FEF0E21
P 4750 3850
F 0 "C2" V 4521 3850 50  0000 C CNN
F 1 "C_Small" V 4612 3850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4750 3850 50  0001 C CNN
F 3 "~" H 4750 3850 50  0001 C CNN
	1    4750 3850
	0    1    1    0   
$EndComp
Connection ~ 4650 3850
$Comp
L power:GND #PWR0106
U 1 1 5FEF2007
P 4850 3850
F 0 "#PWR0106" H 4850 3600 50  0001 C CNN
F 1 "GND" H 4855 3677 50  0000 C CNN
F 2 "" H 4850 3850 50  0001 C CNN
F 3 "" H 4850 3850 50  0001 C CNN
	1    4850 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5FEF23E0
P 4650 4650
F 0 "#PWR0107" H 4650 4400 50  0001 C CNN
F 1 "GND" H 4655 4477 50  0000 C CNN
F 2 "" H 4650 4650 50  0001 C CNN
F 3 "" H 4650 4650 50  0001 C CNN
	1    4650 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5FEF278C
P 4650 2700
F 0 "#PWR0108" H 4650 2450 50  0001 C CNN
F 1 "GND" H 4655 2527 50  0000 C CNN
F 2 "" H 4650 2700 50  0001 C CNN
F 3 "" H 4650 2700 50  0001 C CNN
	1    4650 2700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5FEF403C
P 4650 1900
F 0 "#PWR0109" H 4650 1750 50  0001 C CNN
F 1 "+5V" H 4665 2073 50  0000 C CNN
F 2 "" H 4650 1900 50  0001 C CNN
F 3 "" H 4650 1900 50  0001 C CNN
	1    4650 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2100 4650 1900
$Comp
L Device:C_Small C1
U 1 1 5FEF4043
P 4750 1900
F 0 "C1" V 4521 1900 50  0000 C CNN
F 1 "C_Small" V 4612 1900 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4750 1900 50  0001 C CNN
F 3 "~" H 4750 1900 50  0001 C CNN
	1    4750 1900
	0    1    1    0   
$EndComp
Connection ~ 4650 1900
$Comp
L power:GND #PWR0110
U 1 1 5FEF404A
P 4850 1900
F 0 "#PWR0110" H 4850 1650 50  0001 C CNN
F 1 "GND" H 4855 1727 50  0000 C CNN
F 2 "" H 4850 1900 50  0001 C CNN
F 3 "" H 4850 1900 50  0001 C CNN
	1    4850 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U4
U 1 1 5FE7AF23
P 6000 2400
F 0 "U4" H 6000 2881 50  0000 C CNN
F 1 "74HC74" H 6000 2790 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6000 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 6000 2400 50  0001 C CNN
	1    6000 2400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U4
U 2 1 5FE7B912
P 5850 4350
F 0 "U4" H 5850 4831 50  0000 C CNN
F 1 "74HC74" H 5850 4740 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 5850 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 5850 4350 50  0001 C CNN
	2    5850 4350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U4
U 3 1 5FE7C64A
P 8900 1600
F 0 "U4" H 9130 1646 50  0000 L CNN
F 1 "74HC74" H 9130 1555 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 8900 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 8900 1600 50  0001 C CNN
	3    8900 1600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 5FE7E8D3
P 8900 1000
F 0 "#PWR0111" H 8900 850 50  0001 C CNN
F 1 "+5V" H 8915 1173 50  0000 C CNN
F 2 "" H 8900 1000 50  0001 C CNN
F 3 "" H 8900 1000 50  0001 C CNN
	1    8900 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 1200 8900 1000
$Comp
L Device:C_Small C5
U 1 1 5FE7E8DA
P 9000 1000
F 0 "C5" V 8771 1000 50  0000 C CNN
F 1 "C_Small" V 8862 1000 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9000 1000 50  0001 C CNN
F 3 "~" H 9000 1000 50  0001 C CNN
	1    9000 1000
	0    1    1    0   
$EndComp
Connection ~ 8900 1000
$Comp
L power:GND #PWR0112
U 1 1 5FE7E8E1
P 9100 1000
F 0 "#PWR0112" H 9100 750 50  0001 C CNN
F 1 "GND" H 9105 827 50  0000 C CNN
F 2 "" H 9100 1000 50  0001 C CNN
F 3 "" H 9100 1000 50  0001 C CNN
	1    9100 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5FE7ED4B
P 8900 2000
F 0 "#PWR0113" H 8900 1750 50  0001 C CNN
F 1 "GND" H 8905 1827 50  0000 C CNN
F 2 "" H 8900 2000 50  0001 C CNN
F 3 "" H 8900 2000 50  0001 C CNN
	1    8900 2000
	1    0    0    -1  
$EndComp
Text GLabel 5750 4750 0    50   Input ~ 0
~RESET~
Wire Wire Line
	5750 4750 5850 4750
Wire Wire Line
	5850 4750 5850 4650
Text GLabel 5900 2800 0    50   Input ~ 0
~RESET~
Wire Wire Line
	5900 2800 6000 2800
Wire Wire Line
	6000 2800 6000 2700
$Comp
L 74xx:74LS74 U5
U 1 1 5FE7FAA8
P 7050 2400
F 0 "U5" H 7050 2881 50  0000 C CNN
F 1 "74HC74" H 7050 2790 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 7050 2400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 7050 2400 50  0001 C CNN
	1    7050 2400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U5
U 2 1 5FE80F57
P 6950 4350
F 0 "U5" H 6950 4831 50  0000 C CNN
F 1 "74HC74" H 6950 4740 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6950 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 6950 4350 50  0001 C CNN
	2    6950 4350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS74 U5
U 3 1 5FE81AD1
P 10100 1600
F 0 "U5" H 10330 1646 50  0000 L CNN
F 1 "74HC74" H 10330 1555 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 10100 1600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 10100 1600 50  0001 C CNN
	3    10100 1600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0114
U 1 1 5FE82F73
P 10100 1000
F 0 "#PWR0114" H 10100 850 50  0001 C CNN
F 1 "+5V" H 10115 1173 50  0000 C CNN
F 2 "" H 10100 1000 50  0001 C CNN
F 3 "" H 10100 1000 50  0001 C CNN
	1    10100 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1200 10100 1000
$Comp
L Device:C_Small C6
U 1 1 5FE82F7A
P 10200 1000
F 0 "C6" V 9971 1000 50  0000 C CNN
F 1 "C_Small" V 10062 1000 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10200 1000 50  0001 C CNN
F 3 "~" H 10200 1000 50  0001 C CNN
	1    10200 1000
	0    1    1    0   
$EndComp
Connection ~ 10100 1000
$Comp
L power:GND #PWR0115
U 1 1 5FE82F81
P 10300 1000
F 0 "#PWR0115" H 10300 750 50  0001 C CNN
F 1 "GND" H 10305 827 50  0000 C CNN
F 2 "" H 10300 1000 50  0001 C CNN
F 3 "" H 10300 1000 50  0001 C CNN
	1    10300 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5FE83509
P 10100 2000
F 0 "#PWR0116" H 10100 1750 50  0001 C CNN
F 1 "GND" H 10105 1827 50  0000 C CNN
F 2 "" H 10100 2000 50  0001 C CNN
F 3 "" H 10100 2000 50  0001 C CNN
	1    10100 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2400 5450 2400
Wire Wire Line
	5550 4350 5300 4350
Wire Wire Line
	6300 2300 6750 2300
Wire Wire Line
	6150 4250 6650 4250
$Comp
L 74xx:74LS08 U3
U 1 1 5FE8ECD7
P 5500 1650
F 0 "U3" H 5500 1975 50  0000 C CNN
F 1 "74HC08" H 5500 1884 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 5500 1650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 5500 1650 50  0001 C CNN
	1    5500 1650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U3
U 2 1 5FE910B4
P 5350 3500
F 0 "U3" H 5350 3825 50  0000 C CNN
F 1 "74HC08" H 5350 3734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 5350 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 5350 3500 50  0001 C CNN
	2    5350 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U3
U 3 1 5FE93696
P 8200 2800
F 0 "U3" H 8200 3125 50  0000 C CNN
F 1 "74HC08" H 8200 3034 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 8200 2800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 8200 2800 50  0001 C CNN
	3    8200 2800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U3
U 4 1 5FE96C2A
P 8250 4700
F 0 "U3" H 8250 5025 50  0000 C CNN
F 1 "74HC08" H 8250 4934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 8250 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 8250 4700 50  0001 C CNN
	4    8250 4700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U3
U 5 1 5FE996D3
P 8100 1550
F 0 "U3" H 8330 1596 50  0000 L CNN
F 1 "74HC08" H 8330 1505 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 8100 1550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 8100 1550 50  0001 C CNN
	5    8100 1550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 5FE9DD88
P 8100 850
F 0 "#PWR0117" H 8100 700 50  0001 C CNN
F 1 "+5V" H 8115 1023 50  0000 C CNN
F 2 "" H 8100 850 50  0001 C CNN
F 3 "" H 8100 850 50  0001 C CNN
	1    8100 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1050 8100 850 
$Comp
L Device:C_Small C4
U 1 1 5FE9DD8F
P 8200 850
F 0 "C4" V 7971 850 50  0000 C CNN
F 1 "C_Small" V 8062 850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8200 850 50  0001 C CNN
F 3 "~" H 8200 850 50  0001 C CNN
	1    8200 850 
	0    1    1    0   
$EndComp
Connection ~ 8100 850 
$Comp
L power:GND #PWR0118
U 1 1 5FE9DD96
P 8300 850
F 0 "#PWR0118" H 8300 600 50  0001 C CNN
F 1 "GND" H 8305 677 50  0000 C CNN
F 2 "" H 8300 850 50  0001 C CNN
F 3 "" H 8300 850 50  0001 C CNN
	1    8300 850 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5FE9E35E
P 6700 1550
F 0 "#PWR0119" H 6700 1300 50  0001 C CNN
F 1 "GND" H 6705 1377 50  0000 C CNN
F 2 "" H 6700 1550 50  0001 C CNN
F 3 "" H 6700 1550 50  0001 C CNN
	1    6700 1550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 1 1 5FEA8694
P 6300 4850
F 0 "U1" H 6300 5167 50  0000 C CNN
F 1 "74HC04" H 6300 5076 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6300 4850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 6300 4850 50  0001 C CNN
	1    6300 4850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 2 1 5FEA9098
P 4500 3400
F 0 "U1" H 4500 3717 50  0000 C CNN
F 1 "74HC04" H 4500 3626 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 4500 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 4500 3400 50  0001 C CNN
	2    4500 3400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 3 1 5FEAB234
P 2700 3950
F 0 "U1" H 2700 4267 50  0000 C CNN
F 1 "74HC04" H 2700 4176 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 2700 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 2700 3950 50  0001 C CNN
	3    2700 3950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 4 1 5FEACDF4
P 6450 2900
F 0 "U1" H 6450 3217 50  0000 C CNN
F 1 "74HC04" H 6450 3126 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 6450 2900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 6450 2900 50  0001 C CNN
	4    6450 2900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 5 1 5FEADCFD
P 7000 1400
F 0 "U1" H 7000 1717 50  0000 C CNN
F 1 "74HC04" H 7000 1626 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 7000 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 7000 1400 50  0001 C CNN
	5    7000 1400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 6 1 5FEAF5E8
P 7000 1050
F 0 "U1" H 7000 1367 50  0000 C CNN
F 1 "74HC04" H 7000 1276 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 7000 1050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 7000 1050 50  0001 C CNN
	6    7000 1050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U1
U 7 1 5FEB0771
P 11050 1550
F 0 "U1" H 11280 1596 50  0000 L CNN
F 1 "74HC04" H 11280 1505 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 11050 1550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 11050 1550 50  0001 C CNN
	7    11050 1550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0120
U 1 1 5FEB33EF
P 11050 850
F 0 "#PWR0120" H 11050 700 50  0001 C CNN
F 1 "+5V" H 11065 1023 50  0000 C CNN
F 2 "" H 11050 850 50  0001 C CNN
F 3 "" H 11050 850 50  0001 C CNN
	1    11050 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	11050 1050 11050 850 
$Comp
L Device:C_Small C8
U 1 1 5FEB33F6
P 11150 850
F 0 "C8" V 10921 850 50  0000 C CNN
F 1 "C_Small" V 11012 850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 11150 850 50  0001 C CNN
F 3 "~" H 11150 850 50  0001 C CNN
	1    11150 850 
	0    1    1    0   
$EndComp
Connection ~ 11050 850 
$Comp
L power:GND #PWR0121
U 1 1 5FEB33FD
P 11250 850
F 0 "#PWR0121" H 11250 600 50  0001 C CNN
F 1 "GND" H 11255 677 50  0000 C CNN
F 2 "" H 11250 850 50  0001 C CNN
F 3 "" H 11250 850 50  0001 C CNN
	1    11250 850 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5FEB3AFC
P 11050 2050
F 0 "#PWR0122" H 11050 1800 50  0001 C CNN
F 1 "GND" H 11055 1877 50  0000 C CNN
F 2 "" H 11050 2050 50  0001 C CNN
F 3 "" H 11050 2050 50  0001 C CNN
	1    11050 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2400 6750 2900
Wire Wire Line
	6150 2900 5450 2900
Wire Wire Line
	5450 2900 5450 2400
Connection ~ 5450 2400
Wire Wire Line
	5450 2400 4950 2400
Wire Wire Line
	6600 4850 6600 4350
Wire Wire Line
	6600 4350 6650 4350
Wire Wire Line
	6000 4850 5950 4850
Wire Wire Line
	5300 4850 5300 4350
Connection ~ 5300 4350
Wire Wire Line
	5300 4350 4950 4350
Text GLabel 7050 4750 2    50   Input ~ 0
~RESET~
Wire Wire Line
	7050 4750 6950 4750
Wire Wire Line
	6950 4750 6950 4650
Text GLabel 7150 2800 2    50   Input ~ 0
~RESET~
Wire Wire Line
	7150 2800 7050 2800
Wire Wire Line
	7050 2800 7050 2700
$Comp
L 74xx:74LS173 U2
U 1 1 5FEBF71B
P 3200 2550
F 0 "U2" H 3200 3631 50  0000 C CNN
F 1 "74HC173" H 3200 3540 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 3200 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 3200 2550 50  0001 C CNN
	1    3200 2550
	1    0    0    -1  
$EndComp
Text GLabel 2700 1950 0    50   BiDi ~ 0
D0
$Comp
L power:GND #PWR0123
U 1 1 5FECBB07
P 3200 3450
F 0 "#PWR0123" H 3200 3200 50  0001 C CNN
F 1 "GND" H 3205 3277 50  0000 C CNN
F 2 "" H 3200 3450 50  0001 C CNN
F 3 "" H 3200 3450 50  0001 C CNN
	1    3200 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0124
U 1 1 5FECD3FC
P 3200 1450
F 0 "#PWR0124" H 3200 1300 50  0001 C CNN
F 1 "+5V" H 3215 1623 50  0000 C CNN
F 2 "" H 3200 1450 50  0001 C CNN
F 3 "" H 3200 1450 50  0001 C CNN
	1    3200 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1650 3200 1450
$Comp
L Device:C_Small C3
U 1 1 5FECD403
P 3300 1450
F 0 "C3" V 3071 1450 50  0000 C CNN
F 1 "C_Small" V 3162 1450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3300 1450 50  0001 C CNN
F 3 "~" H 3300 1450 50  0001 C CNN
	1    3300 1450
	0    1    1    0   
$EndComp
Connection ~ 3200 1450
$Comp
L power:GND #PWR0125
U 1 1 5FECD40A
P 3400 1450
F 0 "#PWR0125" H 3400 1200 50  0001 C CNN
F 1 "GND" H 3405 1277 50  0000 C CNN
F 2 "" H 3400 1450 50  0001 C CNN
F 3 "" H 3400 1450 50  0001 C CNN
	1    3400 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 1550 4200 1550
Wire Wire Line
	4200 1550 4200 1950
Wire Wire Line
	4200 1950 3700 1950
Wire Wire Line
	5050 3400 4800 3400
Wire Wire Line
	4200 3400 4200 1950
Connection ~ 4200 1950
Wire Wire Line
	7350 2500 7500 2500
Wire Wire Line
	7500 2500 7500 3200
Wire Wire Line
	7500 3200 4950 3200
Wire Wire Line
	4950 3200 4950 3600
Wire Wire Line
	4950 3600 5050 3600
Wire Wire Line
	5200 1750 5200 3050
Wire Wire Line
	5200 3050 6100 3050
Wire Wire Line
	6100 3050 6100 3550
Wire Wire Line
	6100 3550 7450 3550
Wire Wire Line
	7450 3550 7450 4450
Wire Wire Line
	7450 4450 7250 4450
Wire Wire Line
	5650 3500 5650 3800
Wire Wire Line
	5650 3800 5550 3800
Wire Wire Line
	5550 3800 5550 4250
Wire Wire Line
	5800 1650 5800 2000
Wire Wire Line
	5800 2000 5700 2000
Wire Wire Line
	5700 2000 5700 2300
Text GLabel 2650 2700 0    50   BiDi ~ 0
~SLOT_SEL~
Text GLabel 2700 2950 0    50   Input ~ 0
CLK
Text GLabel 2350 4200 0    50   Input ~ 0
~RESET~
Wire Wire Line
	2350 4200 2400 4200
Wire Wire Line
	2400 4200 2400 3950
Wire Wire Line
	3000 3950 3000 3400
Wire Wire Line
	3000 3400 2700 3400
Wire Wire Line
	2700 3400 2700 3150
$Comp
L power:GND #PWR0126
U 1 1 5FEF077F
P 2450 2400
F 0 "#PWR0126" H 2450 2150 50  0001 C CNN
F 1 "GND" H 2455 2227 50  0000 C CNN
F 2 "" H 2450 2400 50  0001 C CNN
F 3 "" H 2450 2400 50  0001 C CNN
	1    2450 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2550 2700 2450
Connection ~ 2700 2150
Wire Wire Line
	2700 2150 2700 2050
Connection ~ 2700 2250
Wire Wire Line
	2700 2250 2700 2150
Connection ~ 2700 2450
Wire Wire Line
	2700 2450 2700 2350
Wire Wire Line
	2700 2350 2450 2350
Wire Wire Line
	2450 2350 2450 2400
Connection ~ 2700 2350
Wire Wire Line
	2700 2350 2700 2250
Wire Wire Line
	7900 2700 7750 2700
Wire Wire Line
	7750 2700 7750 2300
Wire Wire Line
	7750 2300 7350 2300
Wire Wire Line
	7900 2900 6800 2900
Wire Wire Line
	6800 2900 6800 3150
Wire Wire Line
	6800 3150 6150 3150
Wire Wire Line
	6150 3150 6150 2900
Connection ~ 6150 2900
Wire Wire Line
	7950 4600 7950 4250
Wire Wire Line
	7950 4250 7250 4250
Wire Wire Line
	7950 4800 7550 4800
Wire Wire Line
	7550 4800 7550 5100
Wire Wire Line
	7550 5100 5950 5100
Wire Wire Line
	5950 5100 5950 4850
Connection ~ 5950 4850
Wire Wire Line
	5950 4850 5300 4850
$Comp
L 74xx:74LS32 U6
U 1 1 5FF03FC3
P 9250 3700
F 0 "U6" H 9250 4025 50  0000 C CNN
F 1 "74HC32" H 9250 3934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 9250 3700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 9250 3700 50  0001 C CNN
	1    9250 3700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U6
U 2 1 5FF05BE1
P 9450 4500
F 0 "U6" H 9450 4825 50  0000 C CNN
F 1 "74HC32" H 9450 4734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 9450 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 9450 4500 50  0001 C CNN
	2    9450 4500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U6
U 3 1 5FF0832E
P 9450 5000
F 0 "U6" H 9450 5325 50  0000 C CNN
F 1 "74HC32" H 9450 5234 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 9450 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 9450 5000 50  0001 C CNN
	3    9450 5000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U6
U 4 1 5FF0AFB7
P 9450 5500
F 0 "U6" H 9450 5825 50  0000 C CNN
F 1 "74HC32" H 9450 5734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 9450 5500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 9450 5500 50  0001 C CNN
	4    9450 5500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U6
U 5 1 5FF0CC45
P 10500 3250
F 0 "U6" H 10730 3296 50  0000 L CNN
F 1 "74HC32" H 10730 3205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 10500 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 10500 3250 50  0001 C CNN
	5    10500 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0127
U 1 1 5FF0FC14
P 10500 2550
F 0 "#PWR0127" H 10500 2400 50  0001 C CNN
F 1 "+5V" H 10515 2723 50  0000 C CNN
F 2 "" H 10500 2550 50  0001 C CNN
F 3 "" H 10500 2550 50  0001 C CNN
	1    10500 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 2750 10500 2550
$Comp
L Device:C_Small C7
U 1 1 5FF0FC1B
P 10600 2550
F 0 "C7" V 10371 2550 50  0000 C CNN
F 1 "C_Small" V 10462 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10600 2550 50  0001 C CNN
F 3 "~" H 10600 2550 50  0001 C CNN
	1    10600 2550
	0    1    1    0   
$EndComp
Connection ~ 10500 2550
$Comp
L power:GND #PWR0128
U 1 1 5FF0FC22
P 10700 2550
F 0 "#PWR0128" H 10700 2300 50  0001 C CNN
F 1 "GND" H 10705 2377 50  0000 C CNN
F 2 "" H 10700 2550 50  0001 C CNN
F 3 "" H 10700 2550 50  0001 C CNN
	1    10700 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5FF1207E
P 10500 3750
F 0 "#PWR0129" H 10500 3500 50  0001 C CNN
F 1 "GND" H 10505 3577 50  0000 C CNN
F 2 "" H 10500 3750 50  0001 C CNN
F 3 "" H 10500 3750 50  0001 C CNN
	1    10500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 3600 8950 2800
Wire Wire Line
	8950 2800 8500 2800
Wire Wire Line
	8950 3800 8950 4700
Wire Wire Line
	8950 4700 8550 4700
Text GLabel 9550 3700 2    50   Output ~ 0
CLK
Wire Wire Line
	6700 1550 6700 1400
Connection ~ 6700 1400
Wire Wire Line
	6700 1400 6700 1050
$Comp
L power:GND #PWR0130
U 1 1 5FF28722
P 8100 2050
F 0 "#PWR0130" H 8100 1800 50  0001 C CNN
F 1 "GND" H 8105 1877 50  0000 C CNN
F 2 "" H 8100 2050 50  0001 C CNN
F 3 "" H 8100 2050 50  0001 C CNN
	1    8100 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5FF2BC10
P 6000 1650
F 0 "R1" H 6059 1696 50  0000 L CNN
F 1 "10k" H 6059 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 1650 50  0001 C CNN
F 3 "~" H 6000 1650 50  0001 C CNN
	1    6000 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0131
U 1 1 5FF2C880
P 6000 1400
F 0 "#PWR0131" H 6000 1250 50  0001 C CNN
F 1 "+5V" H 6015 1573 50  0000 C CNN
F 2 "" H 6000 1400 50  0001 C CNN
F 3 "" H 6000 1400 50  0001 C CNN
	1    6000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2100 6000 1850
Wire Wire Line
	6000 1550 6000 1400
Wire Wire Line
	7050 2100 7050 1850
Wire Wire Line
	7050 1850 6650 1850
Connection ~ 6000 1850
Wire Wire Line
	6000 1850 6000 1750
Wire Wire Line
	6950 4050 6950 3750
Wire Wire Line
	6950 2700 6650 2700
Wire Wire Line
	6650 2700 6650 1850
Connection ~ 6650 1850
Wire Wire Line
	6650 1850 6000 1850
Wire Wire Line
	5850 4050 5850 3750
Wire Wire Line
	5850 3750 6950 3750
Connection ~ 6950 3750
Wire Wire Line
	6950 3750 6950 2700
Wire Wire Line
	9150 4400 9150 4600
Connection ~ 9150 4600
Wire Wire Line
	9150 4600 9150 4900
Connection ~ 9150 4900
Wire Wire Line
	9150 4900 9150 5100
Connection ~ 9150 5100
Wire Wire Line
	9150 5100 9150 5400
Connection ~ 9150 5400
Wire Wire Line
	9150 5400 9150 5600
$Comp
L power:GND #PWR0132
U 1 1 5FF4D6B2
P 9150 5700
F 0 "#PWR0132" H 9150 5450 50  0001 C CNN
F 1 "GND" H 9155 5527 50  0000 C CNN
F 2 "" H 9150 5700 50  0001 C CNN
F 3 "" H 9150 5700 50  0001 C CNN
	1    9150 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 5700 9150 5600
Connection ~ 9150 5600
Wire Wire Line
	2700 2750 2700 2700
Wire Wire Line
	2700 2700 2650 2700
Text GLabel 2450 2850 0    50   Input ~ 0
R~W~
Wire Wire Line
	2700 2850 2450 2850
$EndSCHEMATC
