## Clock board

### Obsolete since [the backplane](https://gitlab.com/planck-6502/backplane) has a clock oscillator

This is the clock board for the Planck 6502 based computer
It features 2 crystals which allow dynamic clock switching. Simply by writing a one or a zero to the expansion slot address, one clock or the other will be selected.

The plan is to add a counter that will allow the clock to be divided and thus slowed down further for debugging. 

This is what the board looks like at the moment:

![Clock board 3D view](clock_board.png)


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.